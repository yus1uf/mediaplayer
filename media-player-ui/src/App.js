
import './App.css';
import React from 'react';
import Header from './component/header/Header'
import SearchBar from './component/searchbox/SearchBox';
import Playlist from './component/playlist/Playlist'
import AddMedia from './component/addmedia/AddMedia';

function App() {

  const [searchTerm, setSearchterm] = React.useState("")
  const [searchResults, setSearchResults] = React.useState([])

  const [playlistItems, setItems] = React.useState([
    {
      id: 1,
      like: 3,
      title: "Com Truise - Flightwave",
      subtitle: "Read",
      media: ""
    },
    {
      id: 2,
      like: 3,
      title: "Claude Debussy - Clair de lune",
      subtitle: "Read",
      media: ""
    },
    {
      id: 3,
      like: 2,
      title: "Culture Shock - Troglodyte",
      subtitle: "Doug",
      media: ""
    },
    {
      id: 4,
      like: 2,
      title: "Tycho - Montana",
      subtitle: "Read",
      media: ""
    }
  ]
  );

  const searchHandler = (searchTerm) => {
    setSearchterm(searchTerm)
    if (searchTerm !== "") {
      const newTrack = playlistItems.filter((item) => {
        return Object.values(item)
          .join(" ")
          .toLowerCase()
          .includes(searchTerm.toString().toLowerCase())
      });
      console.log(newTrack)
      setSearchResults(newTrack)
    } else {
      setSearchResults(playlistItems)
    }
  }

  return (
    <div className="App">
      <Header />
      <SearchBar data={playlistItems} term={searchTerm} searchKeyword={searchHandler} />
      <Playlist data={searchTerm.length < 1 ? playlistItems : searchResults} />
      <AddMedia data={playlistItems} setData={setItems} />
    </div>
  );
}

export default App;
