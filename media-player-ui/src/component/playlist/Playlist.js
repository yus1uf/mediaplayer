import React from 'react';
import Like from './Like'
import './playlist.css'
export default class Playlist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            song: this.props.data
        }
        this.deleteData = this.deleteData.bind(this)
    }

    deleteData(param) {

        this.state.song.map((item) => {
            if (item.id === param) {
                this.state.song.splice(item, 1)
            }
        })
        this.setState(this.state.song)
    }

    render() {

        return (
            <div className="play-list">
                {
                    this.props.data.length > 0 ?
                        this.state.song.map((item) =>
                            <div className="list-item">
                                <Like like={item.like} />
                                <div className="title-txt">
                                    <p> {item.title}</p>
                                    <span>{item.subtitle}</span>
                                </div>
                                <audio controls></audio>
                                <div className="delete">
                                    <button><i className="fas fa-chevron-circle-down"></i></button>
                                    <div class="dropdown-content">
                                        <button onClick={() => this.deleteData(item.id)}>Delete</button>
                                    </div>
                                </div>
                            </div>
                        )
                        : <h1>No data Found</h1>
                }
            </div>
        )

    }

}