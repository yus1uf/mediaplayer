import React from "react";
import "./header.css"
export default function Headers() {
    return (
        <div className="header">
            <div className="track">
                <h4>React Track</h4>
            </div>
            <div className="signout">
                <h4>Sign Out</h4>
            </div>
            <h2>Media Player</h2>
        </div>
    )
}